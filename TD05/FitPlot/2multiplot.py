import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt("expe.dat")

plt.plot(
    data[:, 0], data[:, 2], 'ro',
    data[:, 0], data[:, 3], 'bs',
    data[:, 0], data[:, 4], 'g^')

plt.show()
