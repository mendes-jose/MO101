import matplotlib.pyplot as plt
import numpy as np

## chargement d'une base de donnee commune et connue
materiaux_connus = {
    "alu"    : 69e3,
    "cuivre" : 124e3,
    "titane"  : 114e3
}
## idealement, on ferait plutot 
# import materiaux_standards
# materiaux_connus = materiaux_standards.data


## on trace les donnees de l'expe
data=np.loadtxt("expe.dat")
plt.plot ( data[:,1], data[:,3], 'ro')
plt.annotate("expe", (data[-1,1], data[-1,3]),horizontalalignment='right')


## on ajoute le resultat de notre identification au dictionnaire
E_identifie=132613.085622
materiaux_connus["identification"] = E_identifie

## on trace les differentes pentes elastiques pour ces materiaux
x = np.array([0.,0.005])

for mat in materiaux_connus:
    y = x * materiaux_connus[mat]
    plt.plot(x,y)
    plt.annotate(mat, xy=(x[1],y[1]))

plt.show()



