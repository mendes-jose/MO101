import matplotlib.pyplot as plt
import numpy as np

from scipy.optimize import leastsq

## charge les donnnes 
data=np.loadtxt("expe.dat")

## plotte les donnees 
plt.plot(data[:,1], data[:,3], 'bs')
plt.xlabel("$\epsilon_{11}$")
plt.ylabel("$\sigma$")

## fit des 6 premiers points par une fonction lineaire
NPOINTS=5
def fitfunc(E,x):
    "La fonction (lineaire) a fitter"
    return E*x
def errfunc(E,x,y): 
    "La fonction d'erreur : renvoye la distance entre fitfunc et y"
    return fitfunc(E,x) - y 

X = data[:NPOINTS,1]
Y = data[:NPOINTS,3]

E, success = leastsq( errfunc,     # fonction a minimiser
                      0.,          # initial guess 
                      args=(X,Y)   # donnees a fitter 
                      )

if not (success in (1,2,3,4) ) :   # cf. la doc le la fonction leastsq
    raise StandardError("L'optimisation a rate ... ")


## plotte la fonction fittee
X = data[0:NPOINTS,1]
Y = fitfunc(E,X)

plt.plot(X,Y,'k-')


## Affichage du graphique et du module d'elasticite trouve
print "E=", E
plt.show()

