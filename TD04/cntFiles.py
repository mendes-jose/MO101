#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, os.path

# create list of files within the current directory
listOfNames = [name for name in os.listdir('.') if os.path.isfile(name)]

print('There are {} files inside {}'.format(len(listOfNames), os.getcwd()))