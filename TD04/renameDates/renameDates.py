#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# renameDates.py - Renames filenames with American MM-DD-YYYY date format
# to European DD-MM-YYYY.

import shutil, os, re

# Create a regex that matches files with the American date format.
datePattern = re.compile(r"""^(.*?) #alltextbeforethedate
	((0|1)?\d)-						#oneortwodigitsforthemonth
	((0|1|2|3)?\d)-					#oneortwodigitsfortheday
	((19|20)\d\d)					#fourdigitsfortheyear
	(.*?)$ 							#alltextafterthedate
	""",re.VERBOSE)

for amerFilename in os.listdir('.'):
	mo = datePattern.search(amerFilename)
	
	#Skipfileswithoutadate.
	
	if mo == None:
		continue

	# Get the different parts of the filename.

	beforePart = mo.group(1)
	monthPart = mo.group(2)
	dayPart = mo.group(4)
	yearPart = mo.group(6)
	afterPart = mo.group(8)

# Form the European - style filename.
euroFilename = beforePart + dayPart + '-' + monthPart + '-' + yearPart + afterPart

#Getthefull,absolutefilepaths.
absWorkingDir = os.path.abspath('.')
amerFilename = os.path.join(absWorkingDir,amerFilename)
euroFilename = os.path.join(absWorkingDir,euroFilename)

#Renamethefiles.
print('Renaming"%s"to"%s"...'%(amerFilename,euroFilename))
shutil.move(amerFilename,euroFilename) # uncomment after testing