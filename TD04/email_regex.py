#! python3
# phoneAndEmail.py - Finds phone numbers and email addresses on the clipboard.
import re

# phoneRegex = re.compile(r'''(
# --snip--

# Create email regex.
emailRegex = re.compile(r'''(
      [a-zA-Z0-9._%+-]+      # username
      @                      # @ symbol
      [a-zA-Z0-9.-]+         # domain name
   (\.[a-zA-Z]{2,4})      # dot-something
   )''', re.VERBOSE)

# Find matches in text.
text = "This email address is shorter than name.surname@ensta.fr this one here name2.surname2@gmail.com"
matches = []
for match in emailRegex.finditer(text):
	matches.append(match.group(0))

print(matches)