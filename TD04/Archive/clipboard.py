#! python3
   # phoneAndEmail.py - Finds phone numbers and email addresses on the clipboard.

import pyperclip, re

emailRegex = re.compile(r'''(
      [a-zA-Z0-9._%+-]+      # username
      @                      # @ symbol
      [a-zA-Z0-9.-]+         # domain name
   (\.[a-zA-Z]{2,4})      # dot-something
   )''', re.VERBOSE)


# Find matches in clipboard text.
text = str(pyperclip.paste())
matches = []
for groups in phoneRegex.findall(text):
    phoneNum = '-'.join([groups[1], groups[3], groups[5]])
    if groups[8] != '':
        phoneNum += ' x' + groups[8]
    matches.append(phoneNum)
for groups in emailRegex.findall(text):
    matches.append(groups[0])
