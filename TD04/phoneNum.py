#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

phoneRegex = re.compile(r'''(
      \d{3}       # 3 digits
      -           # hyphen
      \d{3}       # 3 digits
      -           # hyphen
      \d{4}       # 4 digits
   )''', re.VERBOSE)

# Find matches in text.
text = "123-123-1234 123-1A3-1234 321-312-5556"
matches = []
for match in phoneRegex.finditer(text):
	matches.append(match.group(0))

print('These are the phone number found: {}'.format(matches))