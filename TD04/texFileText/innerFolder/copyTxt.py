#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import	shutil,	os, re

onlyTxtFilesRegex = re.compile(r'.*[.]txt$')

for	file in os.listdir('.'):

	match = onlyTxtFilesRegex.search(file)
	 
	if match == None:
		continue

	textName = match.group(0)
	print('Moving file "{}" to parent directory.'.format(textName))

	shutil.copy(textName, '../')