#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re


pswdRegex = re.compile(r'''
	[a-za-z0-9@#$%^&+=]		# permited characters
	{8}						# repeated 8 times
	''', re.VERBOSE)


text = "these words are not a passwd, but the password word can be. &2+4567% 12345678 **as1234"

matches = []

for match in pswdRegex.finditer(text):
	print('Found password in input text: {}'.format(match.group(0)))
	# matches.append(groups[0])

# print(matches)