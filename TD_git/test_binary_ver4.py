import pytest
import unittest
from binary import Binary

class Test(unittest.TestCase):

    def test_binary_init_int(self):
        binary = Binary(6)
        assert int(binary) == 6

    def test_binary_init_negative(self):
        with pytest.raises(ValueError):
            binary = Binary(-4)

    def test_binary_init_bitstr(self):
        binary = Binary('110')
        assert int(binary) == 6


    def test_binary_init_binstr(self):
        binary = Binary('0b110')
        assert int(binary) == 6


    def test_binary_init_hexstr(self):
        binary = Binary('0x6')
        assert int(binary) == 6


    def test_binary_init_hex(self):
        binary = Binary(0x6)
        assert int(binary) == 6


    def test_binary_init_intseq(self):
        binary = Binary([1, 1, 0])
        assert int(binary) == 6


    def test_binary_init_strseq(self):
        binary = Binary(['1', '1', '0'])
        assert int(binary) == 6


    # Conversions

    def test_binary_eq(self):
        assert Binary(4) == Binary(4)


    def test_binary_int(self):
        binary = Binary(6)
        assert int(binary) == 6


    def test_binary_bin(self):
        binary = Binary(6)
        assert bin(binary) == '0b110'


    def test_binary_str(self):
        binary = Binary(6)
        assert str(binary) == '110'


    def test_binary_hex(self):
        binary = Binary(6)
        assert hex(binary) == '0x6'


    # Basic operations

    def test_binary_addition_int(self):
        assert Binary(4) + 1 == Binary(5)


    def test_binary_addition_binary(self):
        assert Binary(4) + Binary(5) == Binary(9)


    def test_binary_subtraction_int(self):
        assert Binary(4) - 1 == Binary(3)


    def test_binary_subtraction_binary(self):
        assert Binary(5) - Binary(4) == Binary(1)


    def test_binary_multiplication_int(self):
        assert Binary(5) * 4 == Binary(20)


    def test_binary_multiplication_binary(self):
        assert Binary(5) * Binary(6) == Binary(30)


    def test_binary_division_int(self):
        assert Binary(20) / 4 == Binary(5)


    def test_binary_division_rem_int(self):
        assert Binary(21) / 4 == Binary(5)


    def test_binary_division_binary(self):
        assert Binary(20) / Binary(5) == Binary(4)


    def test_binary_division_rem_binary(self):
        assert Binary(21) / Binary(5) == Binary(4)


    def test_binary_get_bit(self):
        binary = Binary('0101110001')
        assert binary[0] == '1'
        assert binary[5] == '1'


    def test_binary_negative_index(self):
        assert Binary('0101110001')[-1] == '1'
        assert Binary('0101110001')[-2] == '0'


    def test_binary_illegal_index(self):
        with pytest.raises(IndexError):
            Binary('01101010')[7]


    def test_binary_inappropriate_type_index(self):
        with pytest.raises(TypeError):
            Binary('01101010')['key']


    def test_binary_for_loop(self):
        assert [int(i) for i in Binary('01101010')] == [0, 1, 0, 1, 0, 1, 1]


    def test_binary_not(self):
        assert ~Binary('1101') == Binary('10')


    def test_binary_and(self):
        assert Binary('1101') & Binary('1') == Binary('1')


    def test_binary_or(self):
        assert Binary('1101') | Binary('1') == Binary('1101')


    def test_binary_or(self):
        assert Binary('1101') ^ Binary('1') == Binary('1100')


    def test_binary_shl(self):
        assert Binary('1101') << 1 == Binary('11010')


    def test_binary_shl_pos(self):
        assert Binary('1101') << 5 == Binary('110100000')


    def test_binary_shr(self):
        assert Binary('1101') >> 1 == Binary('110')


    def test_binary_shr_pos(self):
        assert Binary('1101') >> 5 == Binary('0')


    # Slicing

    def test_binary_slice(self):
        assert Binary('01101010')[0:3] == Binary('10')
        assert Binary('01101010')[1:4] == Binary('101')
        assert Binary('01101010')[4:] == Binary('110')


    # Split

    def test_binary_split_no_remainder(self):
        assert Binary('110').split(4) == (0, Binary('110'))


    def test_binary_split_remainder(self):
        assert Binary('110').split(2) == (1, Binary('10'))


    def test_binary_split_exact(self):
        assert Binary('100010110').split(9) == (0, Binary('100010110'))


    def test_binary_split_leading_zeros(self):
        assert Binary('100010110').split(8) == (Binary('1'), Binary('10110'))

if __name__ == '__main__':
    unittest.main()
